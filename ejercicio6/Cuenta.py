#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Creacion de la clase Cuenta
class Cuenta():
    # Constructor por parametros
    def __init__(self):
        self.__nombre = None
        self.__cuenta = None
        self.__saldo = None

    # Obtenemos los parametros en privados
    def get_nombre(self):
        return self.__nombre

    def get_cuenta(self):
        return self.__cuenta

    def get_saldo(self):
        return self.__saldo

    # Los definimos dentro de la clase para utilizarlos
    def set_nombre(self, nombre):
        self.__nombre = nombre

    def set_saldo(self, saldo):
        self.__saldo = saldo

    def set_cuenta(self, cuenta):
        self.__cuenta = cuenta

    # Metodo para hacer depositos
    def deposito(self, cantidad):
        # Solo agregamos la cantidad que deseamos depositar
        self.__saldo = self.__saldo + cantidad
        print("{0} es el nuevo saldo \n\n".format(self.__saldo))
        indicador = True
        return self.__saldo, indicador

    # Metodo para realizar giros
    def giro(self, retiro):
        # Necesitamos validar si lo que queremos retirar es menor a nuestro
        # saldo disponible en la cuenta
        if retiro > self.__saldo:
            print("No hay fondos suficientes")
            indicador = False
            return indicador
        else:
            # Si es posible, realizamos el giro y restamos el nuevo saldo
            self.__saldo = self.__saldo - retiro
            print("{0} es el nuevo saldo \n\n".format(self.__saldo))
            indicador = True
            return self.__saldo, indicador

    # Metodo para realizar una transferencia
    def transferencia(self, cuentaDestino, importe):
        # Necesitamos verificar que el importe sea menor al saldo
        if importe < self.__saldo:
            # Realizamos la transferencia
            cuentaDestino = cuentaDestino + importe
            self.__saldo = self.__saldo - importe
            return cuentaDestino
        else:
            print("No posee saldo suficiente")
