#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Cartera import Cartera
from Amigos import Amigos


# Funcion principal
def main():
    # Pedimos el total de amigos
    numero = int(input("Ingrese el numero de amigos: "))
    total = 0
    amigos = []
    # Calculamos los datos de la nueva cuenta
    cuenta = int(input("Ingresar el total de la cuenta: $ "))
    nueva_cuenta = (cuenta * 0.29) + (cuenta * 0.1) + cuenta
    individual = int(nueva_cuenta / numero)
    # Recorremos el total de amigos y pedimos sus datos
    for i in range(numero):
        nombre = str(input("Nombre: "))
        nombre = nombre.upper()
        saldo = int(input("Saldo disponible: $"))
        # Creamos una clase en amigos de cada uno
        nombre = Amigos(nombre, saldo)
        # Si esta poniendo menos del saldo minimo para pagar su parte
        # se le pedirá mas dinero
        if individual > nombre.get_saldo():
            print("No tiene dinero suficiente para pagar su parte. Por ello"
                  " ponga más de su parte")
            saldo = int(input("Esperando su verdadero saldo: $"))
            nombre = Amigos(nombre, saldo)
        # Si no es asi podremos pagar su parte y descontarla de su saldo
        else:
            nombre.pagar_cuenta(individual)
            total = individual + total
            amigo = nombre.nombre.upper()
            amigos.append(amigo)

    # Creamos nuestra billetera
    billetera = Cartera()
    # Adjuntamos nuestro dinero que poseemos
    billetera.set_total(total)
    # Validamos si es posible pagar la cuenta
    validacion = billetera.pagar(nueva_cuenta)
    # Si no es posible pagarla
    if validacion is True:
        print("Felicidades han pagado la cuenta con exito!")
    else:
        # Definimos la nueva cuenta
        pagado = nueva_cuenta - total
        print(pagado)
        billetera.set_diferencia(pagado)
        # Pedimos quien pagará
        print("Ingrese quien pagará lo que falta: ")
        nombre = str(input(""))
        nombre = nombre.upper()
        # Buscamos al amigo
        for i in amigos:
            if i == nombre:
                # Imprimimos la deuda
                deuda = billetera.get_diferencia()
                print("La deuda es: $", deuda)
                # Asumimos que no la poseía en el saldo así que la pedimos
                diferencia = int(input("Ingresar la diferencia: $ \n"))
                # Y generamos condiciones
                if diferencia < deuda:
                    print("No puede pagar la cuenta, vayan a lavar platos\n")
                    break
                else:
                    # Si pone mas que la diferencia, podrán irse
                    nuevo_saldo = Cartera()
                    nuevo_saldo.set_total(diferencia + total)
                    validacion = nuevo_saldo.pagar(nueva_cuenta)


if __name__ == "__main__":
    main()
