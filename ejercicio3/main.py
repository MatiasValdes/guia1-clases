#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Seguridad import Seguridad


# Funcion que hace las transformaciones del numero
def transformaciones(lista):
    lista_temporal = []
    lista_final = []
    encriptado = []
    suma = 0
    suma2 = 0
    # Primero realizaremos la sumatoria de digitos sacamos el modulo para
    # obtener la ultima cifra
    for i in range(len(lista)):
        suma = (suma + int(lista[i])) % 10
        lista_temporal.append(suma)

    # print(lista_temporal)
    # Luego le sumamos a cada digito 7 y nuevamente debemos sacar el modulo
    for i in range(len(lista_temporal)):
        suma2 = 0
        suma2 = (7 + int(lista_temporal[i])) % 10
        lista_final.append(suma2)
        encriptado.append(0)

    # print(lista_final)
    # Ordenamiento básico de los numeros según lo que nos piden
    # Intercambiamos el primero con el tercero y segundo con el cuarto
    encriptado[0] = lista_final[2]
    encriptado[1] = lista_final[3]
    encriptado[2] = lista_final[0]
    encriptado[3] = lista_final[1]
    # print(encriptado)
    # Obtenemos el numero final
    final = ((encriptado[0] * 1000) + (encriptado[1] * 100) +
             (encriptado[2] * 10) + (encriptado[3] * 1))
    print(str(final))
    return final


# Funcion principal
def main():
    # Pedimos el numero de 4 digitos
    numero = int(input("Ingrese digito de 4 cifras:"))
    # Nos aseguramos de que el digito tenga 4 cifras
    if numero >= 1000 and numero < 10000:
        seguridad = Seguridad()
        # Ingresamos nuestro numero
        seguridad.set_digito(numero)
        # A través de dos pasos convertimos el numero en una lista de tipo str
        string = str(numero)
        lista = list(string)
        # Llamamos a la función que nos harán las transformaciones
        transformado = transformaciones(lista)
        # Ingresamos nuestro nuevo numero encriptado
        seguridad.set_secreto(transformado)
        # Imprimimos los datos
        print("{0} fue el numero inicial que ud ingreso.\n{1} es el numero "
              "encriptado".format(seguridad.get_digito(),
                                  seguridad.get_secreto()))
    else:
        print("Ingresar numero valido")


if __name__ == '__main__':
    main()
