#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Creamos la clase seguridad que almacenará nuestros datos en privado
class Seguridad():
    # Constructor por defecto y privado
    def __init__(self):
        self.__inicio = None
        self.__final = None

    # Metodo para obtener el digito normal
    def get_digito(self):
        return self.__inicio

    # Metodo para obtener el digito encriptado
    def get_secreto(self):
        return self.__final

    # Metodo para definir el numero normal
    def set_digito(self, inicio):
        self.__inicio = inicio

    # Metodo para definir el numero encriptado
    def set_secreto(self, final):
        self.__final = final
