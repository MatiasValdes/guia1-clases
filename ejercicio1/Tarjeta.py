#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Creamos la clase Tarjeta
class Tarjeta():
    # Constructor inicial en privado
    def __init__(self):
        self.__nombre = None
        self.__saldo = None

    # Obtenemos el nombre
    def get_nombre(self):
        return self.__nombre

    # Obtenemos el saldo
    def get_saldo(self):
        return self.__saldo

    # Definimmos un nombre y un saldo posteriormente
    def set_nombre(self, nombre):
        self.__nombre = nombre

    def set_saldo(self, saldo):
        self.__saldo = saldo

    # Funcion dento de la clase para cambiar el saldo de la persona
    def saldo(self, compra):
        if compra > self.__saldo:
            print("No hay fondos suficientes")
        else:
            self.__saldo = self.__saldo - compra
            print("{0} es el nuevo saldo.\n\n".format(self.__saldo))
            return (self.__saldo)
