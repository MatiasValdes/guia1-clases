#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from Biblioteca import Biblioteca
from Libro import Libro


# Funcion que busca el libro deseado
def buscar(Biblio):
    # Pedimos el libro
    busqueda = str(input("Ingrese libro a buscar: "))
    busqueda = busqueda.capitalize()
    largo = len(Biblio.libros)
    # Obtenemos las listas generadas en la clase biblioteca
    libros, autores, total, prestados = Biblio.get_listas()
    largo = len(libros)
    # Buscamos el libro
    for i in range(largo):
        if busqueda == libros[i]:
            print("{0} del autor {1} posee un total de {2} pero se encuentran"
                  " prestados {3} copias".format(libros[i], autores[i],
                                                 total[i], prestados[i]))


# Funcion principal
def main():
    # Creamos nuestra biblioteca
    Biblio = Biblioteca()
    print("Bienvenido a la Biblioteca")
    largo = int(input("Ingrese cuantos libros desea agregar: "))
    # Hacemos un ciclo dependiendo de los libros que quiera ingresar
    for i in range(largo):
        # Pedimos los datos
        print("Libro", i + 1)
        titulo = str(input("Ingresar nombre del libro: "))
        titulo = titulo.capitalize()
        autor = str(input("Ingresar autor del libro: "))
        autor = autor.capitalize()
        total = int(input("Ingrese el total de copias del libro: "))
        prestados = int(input("Ingrese el total de copias prestadas: "))
        # Generamos el libro
        i = Libro(titulo, autor, total, prestados)
        # Agregamos a la biblioteca los datos del libro
        Biblio.total_libros(i.get_titulo())
        Biblio.agregar_libro(i.get_total())
        Biblio.libros_prestados(i.get_prestados())
        Biblio.total_autores(i.get_autor())
    # Listamos los libros actualmente en la biblioteca
    print("Los libros actuales de la biblioteca son: \n")
    Biblio.listar_libros()
    # Llamamos la funcion de buscar libros
    buscar(Biblio)


if __name__ == "__main__":
    main()
