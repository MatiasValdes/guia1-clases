#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Perro import Perro


# Funcion con las acciones
def acciones(Chiguagua, nuevo_perro, sexo):
    indicador = 0
    while indicador != 10:
        print("Presione-.\n1)Su perro ladre\n2)Su perro juegue\n3)Su perro"
              " coma\n4)Ver si el nuevo perro peleará con el antiguo"
              "\n5)Ver si el perro tuvo necesidades\n6)Si el perro esta"
              " durmiendo\n7)Cerrar")
        indicador = int(input(""))
        # Segun el numero el perro realizará acciones que están definidas
        # en la clase
        if indicador == 1:
            nuevo_perro.ladrar()
        elif indicador == 2:
            nuevo_perro.jugar()
        elif indicador == 3:
            nuevo_perro.comer()
        elif indicador == 4:
            Chiguagua.pelea(sexo)
        elif indicador == 5:
            nuevo_perro.necesidades()
        elif indicador == 6:
            nuevo_perro.dormir()
        else:
            print("Finalizamos las acciones")
            indicador = 10


# Funcion principal
def main():
    # Definimos un perro por defecto
    Chiguagua = Perro("Chiguagua", "Café", 20, "Macho", 34)
    print("Ingrese las caracteristicas del nuevo perro de la familia")
    # Nuevo perro
    raza = str(input("La raza es: "))
    color = str(input("El color es: "))
    edad = str(input("La edad es:  "))
    sexo = str(input("Es macho o hembra? "))
    peso = str(input("Ingrese el peso  "))
    nuevo_perro = Perro(raza, color, edad, sexo, peso)
    # Acciones que puede realizar el nuevo perro
    acciones(Chiguagua, nuevo_perro, sexo)


if __name__ == "__main__":
    main()
