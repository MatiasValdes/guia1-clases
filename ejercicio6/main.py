#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Importamos la clase Cuenta
from Cuenta import Cuenta
import random


# Generamos dos cuentas al azar
def cuentas_random():
    # Creamos una lista con varios nombres
    nombres = ["Jose", "Matias", "Pedro", "Claudio", "Juan", "Roberto"
               "Camila", "Fernanda", "Tamara"]

    # Realizamos un ciclo para realizar dos cuentas
    for i in range(0, 2):
        # Extraemos datos al azar y agregamos a nuestras cuentas
        nombre = random.choice(nombres)
        saldo = random.randint(0, 1000)
        cuenta = random.randint(1000, 100000)
        if i == 0:
            cuenta1 = Cuenta()
            cuenta1.set_nombre(nombre)
            cuenta1.set_saldo(saldo)
            cuenta1.set_cuenta(cuenta)
        elif i == 1:
            cuenta2 = Cuenta()
            cuenta2.set_nombre(nombre)
            cuenta2.set_saldo(saldo)
            cuenta2.set_cuenta(cuenta)
        else:
            pass

    # Retornamos las cuentas
    return cuenta1, cuenta2


# Funcion principal
def main():
    # Pedimos datos de la cuenta del usuario
    nombre = str(input("Ingrese el nombre del dueño: \n"))
    cuenta = int(input("Ingrese el numero de cuenta: \n"))
    saldo = int(input("Y su saldo inicial:\n"))

    # Llamamos la clase Cuenta ademas de definir dos cuentas por defecto
    persona = Cuenta()
    persona.set_nombre(nombre)
    persona.set_saldo(saldo)
    persona.set_cuenta(cuenta)

    cuenta1, cuenta2 = cuentas_random()

    indicador = 0
    while indicador != 10:
        # Indicamos las posibles acciones pedidas en el ejercicio
        print("Que operaciones desea realizar. Presione: \n"
              "1-. Realizar un deposito\n2-. Realizar un giro\n"
              "3-. Realizar una transferencia\n4-. Cerrar sesión\n")
        indicador = int(input(""))
        if indicador == 1:
            # Depositos
            cantidad = int(input("Ingrese la cantidad del deposito:  "))
            funciona = persona.deposito(cantidad)
            print(funciona)

        elif indicador == 2:
            # Retiro de dinero
            retiro = int(input("Ingrese la cantidad del giro:  "))
            funciona = persona.giro(retiro)
            print(funciona)

        elif indicador == 3:
            # Transferencias
            print("Existen dos cuentas por defecto para realizar"
                  " transferencia")
            print("1-.{0}, numero de cuenta {1} y {2}".format
                  (cuenta1.get_nombre(), cuenta1.get_cuenta(),
                   cuenta1.get_saldo()))
            print("2-.{0}, numero de cuenta {1} y {2}".format
                  (cuenta2.get_nombre(), cuenta2.get_cuenta(),
                   cuenta2.get_saldo()))

            cuenta = int(input("Ingrese cuenta de destino:  "))
            deposito = int(input("Ingrese el deposito total: "))
            # Vemos hacia que cuenta quiere hacer la transferencia
            if cuenta == 1:
                # Realizamos la transferencia y depositamos este nuevo dinero
                # en la cuenta que se trasnfirió
                total = persona.transferencia(cuenta1.get_saldo(), deposito)
                cuenta1.deposito(total)
                print("Saldo {0} de {1}".format(cuenta1.get_saldo(),
                      cuenta1.get_nombre()))
                print("Saldo {0} de {1}".format(persona.get_saldo(),
                      persona.get_nombre()))

            else:
                total = persona.transferencia(cuenta2.get_saldo(), deposito)
                cuenta2.deposito(total)
                print("Saldo {0} de {1}".format(cuenta2.get_saldo(),
                      cuenta2.get_nombre()))
                print("Saldo {0} de {1}".format(persona.get_saldo(),
                      persona.get_nombre()))

        else:
            # Sino simplemente salimos del programa
            indicador = 10

    print("Muchas gracias por utlizar.")


if __name__ == "__main__":
    main()
