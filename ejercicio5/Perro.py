#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Creacion de la clase Perro
class Perro():
    # Iniciamos por parametros iniciales
    def __init__(self, raza, color, edad, sexo, peso):
        self.raza = raza
        self.color = color
        self.edad = edad
        self.sexo = sexo
        self.peso = peso

    # Metodo para que el perro ladre
    def ladrar(self):
        print("Guauuuuuuuu dice mi perro, guau guau ta enojao\n")

    # Metodo para que el perro juegue
    def jugar(self):
        print("****Jugando****\n")

    # Metodo para que el perro coma
    def comer(self):
        print("Comiendo tranquilamente de un plato\n")

    # Metodo que identifica si el nuevo perro peleará con el perro antiguo
    def pelea(self, sexo1):
        sexo2 = self.sexo
        sexo1 = sexo1.upper()
        sexo2 = sexo2.upper()
        if sexo1 != sexo2:
            print("No se pelearán por el terreno\n")
        else:
            print("Se están peleando por el terreno, cuidado!\n")

    # Metodo que nos indica que el perro tuvo necesidades
    def necesidades(self):
        print("No aguantaba y tuve que hacer pipi en el sillón")

    # Metodo de que el perro está durmiendo
    def dormir(self):
        print("****Durmiendo tranquilamente****")
