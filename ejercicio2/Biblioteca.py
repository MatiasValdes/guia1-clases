#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# Creamos la clase biblioteca
class Biblioteca():
    # Constructor por defecto
    def __init__(self):
        # Creamos listas para almacenar datos
        self.cantidad = []
        self.prestados = []
        self.libros = []
        self.autores = []

    # Metodo para ir agregando la cantidad del libro
    def agregar_libro(self, total):
        self.cantidad.append(total)

    # Metodo para agregar cuantos libros se habian prestado
    def libros_prestados(self, prestado):
        self.prestados.append(prestado)

    # Metodo para almacenar los nombres de los libos
    def total_libros(self, libro):
        self.libros.append(libro)

    # Metodo para almacenar los autores de los libros
    def total_autores(self, autor):
        self.autores.append(autor)

    # Metodo que lista los libros de la Biblioteca
    def listar_libros(self):
        for i in self.libros:
            print("{0}".format(i))

    # Metodo para obtener las listas generadas
    def get_listas(self):
        return self.libros, self.autores, self.cantidad, self.prestados
