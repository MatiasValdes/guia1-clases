#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Creacion de la clase cartera que representará el total de dinero de los
# amigos
class Cartera():
    def __init__(self):
        self.__disponible = None
        self.__diferencia = None

    # Metodo para obtener el dinero
    def get_total(self):
        return self.__disponible

    # Metodo para asignar el total de dinero
    def set_total(self, disponible):
        self.__disponible = disponible

    # Metodo para pagar la cuenta y validar si es posible
    def pagar(self, cuenta_final):
        if self.__disponible < cuenta_final:
            return False
        else:
            return True

    # Metodo para asignar la diferencia luego de haber pagado lo que podían
    def set_diferencia(self, mitad_cuenta):
        self.__diferencia = mitad_cuenta

    # Obtenemos esa diferencia
    def get_diferencia(self):
        return self.__diferencia
