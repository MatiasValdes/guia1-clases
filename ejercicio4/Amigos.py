#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Creacion de la clase Amigos
class Amigos():
    # Cronstructor por parametros
    def __init__(self, nombre, saldo):
        self.nombre = nombre
        self.saldo = saldo

    # Metodo para obtener su saldo
    def get_saldo(self):
        return self.saldo

    # Metodo para pagar parte de la cuenta que le corresponde
    def pagar_cuenta(self, parte):
        self.saldo = self.saldo - parte
        return self.saldo
