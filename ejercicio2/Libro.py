#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Definimos la clase libro
class Libro():
    # Se inicia con un constructor por parametros y en privado
    def __init__(self, titulo, autor, total, prestados):
        self.__titulo = titulo
        self.__autor = autor
        self.__total = total
        self.__prestados = prestados

    # metodo para obtener el titulo
    def get_titulo(self):
        return self.__titulo

    # Metodo para obtener el autor
    def get_autor(self):
        return self.__autor

    # Metodo para obtener el total de libros
    def get_total(self):
        return self.__total

    # Metodo para obtener el total de libros prestados
    def get_prestados(self):
        return self.__prestados
