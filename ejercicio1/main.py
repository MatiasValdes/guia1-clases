#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Llamamos a la clase Tarjeta
from Tarjeta import Tarjeta


# Funcion para cambiar el saldo
def cambio_saldo(persona, nombre, saldo):
    # Preguntamos si queremos cambiar el saldo
    indicador = int(input("¿Realizo alguna compra?. Presione:\n1-Si \n"
                    "2-No \n"))
    if indicador == 1:
        retiro = int(input("Ingrese la cantidad utilizada en la compra: $"))
        # Llamamos directamente la funcion dentro de la clase
        persona.saldo(retiro)
    else:
        print("No realizo ninguna compra por lo que queda con {0} de"
              " saldo".format(saldo))


# Funcion principal
def main():
    # Pedimos datos
    nombre = str(input("Ingrese el nombre de la persona:\n"))
    saldo = int(input("Ingrese el saldo de la persona: $ \n"))
    # Llamamos a la clase
    persona = Tarjeta()
    persona.set_nombre(nombre)
    persona.set_saldo(saldo)
    # Datos iniciales
    print("{0} es el saldo de {1} inicial".format(persona.get_saldo(),
          persona.get_nombre()))
    # Fucion que cambia el saldo
    cambio_saldo(persona, nombre, saldo)


if __name__ == "__main__":
    main()
